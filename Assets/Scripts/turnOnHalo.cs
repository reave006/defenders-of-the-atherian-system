﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turnOnHalo : MonoBehaviour {

	public IEnumerator flashHalo()
	{
		Behaviour halo = (Behaviour)GetComponent ("Halo");
		halo.enabled = true;
		yield return new WaitForSeconds (1);
		halo.enabled = false;
	}
}
