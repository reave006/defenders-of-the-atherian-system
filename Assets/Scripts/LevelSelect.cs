﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {

	public Text UnableToLoad;

	private bool instructionsAreVisible = false;
	private GameObject instructions;

	private GameObject[] titleTextBoxes;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	public void Start()
	{
		instructions = GameObject.FindGameObjectWithTag("Instructions");
		instructions.active = false;

		titleTextBoxes = GameObject.FindGameObjectsWithTag("Title");
		
	}

	public void onClickButton1(){
		UnityEngine.SceneManagement.SceneManager.LoadScene ("firstPlanet");
	}

	public void onClickButton2(){
		if (GameObject.Find ("LevelRestriction").GetComponent<AccessToLevels> ().accessToPlanet2) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("SecondPlanet");
		} else {
			UnableToLoad.text = "You must save the first planet before you can move on to the second";
		}
			
	}

	public void onClickButton3(){
		if (GameObject.Find ("LevelRestriction").GetComponent<AccessToLevels> ().accessToPlanet2) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("ThirdPlanet");
		}else {
			UnableToLoad.text = "You must save the second planet before you can move on to the third";
		}
	}

	public void onExitClick()
	{
		Application.Quit();
	}

	public void onClickInstructions()
	{
		// Toggle visibility state.
		instructionsAreVisible = ! instructionsAreVisible;
		if (instructionsAreVisible)
		{
			instructions.active = true;
			TitleVisibility(false);
		}
		else
		{
			instructions.active = false;
			TitleVisibility(true);
		}
	}

	private void TitleVisibility(bool isVisible)
	{
		foreach(GameObject titleTextBox in titleTextBoxes)
		{
			titleTextBox.active = isVisible;
		}
	}
}