﻿using UnityEngine;
using System.Collections;




public class BulletTower : MonoBehaviour {
	
	[Header("Attributes")]

	public float range = 15f;
	public float fireRate = 1f;
	private float fireCountdown = 0f;

	[Header("Unity Setup Fields")]
	private Transform target; 
	//public Transform rotatePart;
	//public float turnSpeed = 50f;

	public GameObject bulletPrefab;
	public Transform bulletSpawnPoint;



	void Start()
	{
		InvokeRepeating ("UpdateTarget", 0f, 0.5f);

	}

	void UpdateTarget()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		foreach (GameObject enemy in enemies) 
		{
			float distance = Vector3.Distance (transform.position, enemy.transform.position);
			if (distance < shortestDistance) 
			{
				shortestDistance = distance;
				nearestEnemy = enemy;
			}
		}

		if (nearestEnemy != null && shortestDistance <= range) {
			target = nearestEnemy.transform;
		} else {
			target = null;
		}
	}
	void FixedUpdate()
	{
		if (target == null)
			return;

		if (fireCountdown <= 0f) {
			Shoot ();
			fireCountdown = 1f / fireRate;
	}

		fireCountdown -= Time.deltaTime;


		}

	void Shoot()
	{
		GameObject bulletGO = (GameObject)Instantiate (bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
		BulletBehavior bullet = bulletGO.GetComponent<BulletBehavior> ();


		if (bullet != null)
			bullet.SeekEnemy (target);
		
	}
}



