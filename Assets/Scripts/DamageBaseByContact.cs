﻿/// <summary>
/// Detects when enemies have made it to the base and deducts health from base.
///
/// @author Allan Haywood
/// </summary>
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SWS;

/// <summary>
/// Detects when enemies have made it to the base and deducts health from base.
/// </summary>
public class DamageBaseByContact : MonoBehaviour 
{
	public GameObject enemyArrival;
	public int decreaseOnEnemyAmount;
	public GameObject explosion;
	public float moveX,moveY,moveZ;
	public Text failed;
	public Button failedButton;

	private GameController gameController;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        // If the game controller is not null, then assign it to our local variable.
        // If the game controller is null, then log an error.
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null)
		{
			Debug.Log("Cannot find 'GameController' script");
		}		
	}

	/// <summary>
    /// When something colides with the base, if it is an enemy, destroy the enemy and deduct health from base. 
    /// </summary>
    /// <param name="other">The object that collidated with the base.</param>
    void OnTriggerEnter(Collider other)
    {
		// If the object is an enemy, destroy the enemy object, decrease health on base.
        if (other.tag == "Enemy")
		{
			StartCoroutine(arrivalEnemy(other));

			gameController.DecreaseHealth(decreaseOnEnemyAmount);

			if (gameController.baseHealth <= 0)
			{
				Vector3 explosionLocation = new Vector3(transform.position.x+moveX,transform.position.y+moveY,transform.position.z+moveZ);
				Quaternion explosionRotation = transform.rotation;

				gameObject.SetActive (false);

				Instantiate (explosion,explosionLocation,explosionRotation);
				Instantiate (explosion,explosionLocation,explosionRotation);
				Instantiate (explosion,explosionLocation,explosionRotation);
				Instantiate (explosion,explosionLocation,explosionRotation);
				Instantiate (explosion,explosionLocation,explosionRotation);
				Instantiate (explosion,explosionLocation,explosionRotation);

				failed.GetComponent<Text> ().enabled = true;
				failedButton.GetComponent<Button>().enabled = true;
				failedButton.GetComponent<Image>().enabled = true;
				failedButton.GetComponentInChildren<Text> ().enabled = true;
			}
        }
    }


	void Update()
	{
		if(Input.GetKeyDown(KeyCode.B)){
			Vector3 explosionLocation = new Vector3(transform.position.x+moveX,transform.position.y+moveY,transform.position.z+moveZ);
			Quaternion explosionRotation = transform.rotation;

			gameObject.SetActive (false);

			Instantiate (explosion,explosionLocation,explosionRotation);
			Instantiate (explosion,explosionLocation,explosionRotation);
			Instantiate (explosion,explosionLocation,explosionRotation);
			Instantiate (explosion,explosionLocation,explosionRotation);
			Instantiate (explosion,explosionLocation,explosionRotation);
			Instantiate (explosion,explosionLocation,explosionRotation);

			failed.GetComponent<Text> ().enabled = true;
			failedButton.GetComponent<Button>().enabled = true;
			failedButton.GetComponent<Image>().enabled = true;
			failedButton.GetComponentInChildren<Text> ().enabled = true;
		}
	}

	IEnumerator arrivalEnemy(Collider enemy)
	{

		enemy.GetComponent<splineMove>().ChangeSpeed(0);
		Vector3 effectLocation = enemy.gameObject.transform.position;
		Quaternion effectRotation = enemy.gameObject.transform.rotation;

		Instantiate(enemyArrival, effectLocation, effectRotation);

		yield return new WaitForSeconds(1);

		if (enemy != null) 
		{
			Destroy(enemy.gameObject);
		}
	}
		
}
