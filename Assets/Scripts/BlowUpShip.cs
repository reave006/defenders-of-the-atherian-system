﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlowUpShip : MonoBehaviour {

	public GameObject explosion;
	public GameObject particleShower;
	public float explosionSize=100;

	public void destroyTheShipWithFireball()
	{
		explosion.gameObject.transform.localScale.Scale (new Vector3 (explosionSize,explosionSize,explosionSize));
		Instantiate (explosion, gameObject.transform.position, gameObject.transform.rotation);
		Instantiate (particleShower, gameObject.transform.position, gameObject.transform.rotation);
		//Instantiate (explosion, gameObject.transform.position, gameObject.transform.rotation);
		Destroy (gameObject);
	}

}
