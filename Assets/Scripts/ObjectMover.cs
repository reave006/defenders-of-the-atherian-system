﻿using UnityEngine;
using System.Collections;


public class ObjectMover : MonoBehaviour {

	public float MoveSpeed=10.0F;
	public GameObject HomePlanet;
	public float maxDistanceFromPlanet =75;
	public float ScrollWheelSpeed=150.0F;
	public float divider=1;
	public bool hasWon=false;
	public GameObject starMove;

	// Use this for initialization
	void Start () {
		//Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (hasWon == false) {
			Vector3 directionOfPlanet = HomePlanet.transform.position - this.transform.position;
			var distToPlanet = Vector3.Distance (HomePlanet.transform.position, this.transform.position);

			float TowardsPlanet = Input.GetAxis ("Mouse ScrollWheel") * ScrollWheelSpeed;
			float SidewaysAcrossPlanet = Input.GetAxis ("Horizontal") * MoveSpeed;
			float UpAndDownAlongPlanet = Input.GetAxis ("Vertical") * MoveSpeed;

			if (Input.GetKey (KeyCode.X)) {
				TowardsPlanet = 1;
			}
			if (Input.GetKey (KeyCode.Z)) {
				TowardsPlanet = -1;
			}

			TowardsPlanet *= Time.deltaTime;
			SidewaysAcrossPlanet *= Time.deltaTime;
			UpAndDownAlongPlanet *= Time.deltaTime;
			TowardsPlanet *= (distToPlanet / divider);

			transform.LookAt (HomePlanet.transform);
			transform.Translate (SidewaysAcrossPlanet, UpAndDownAlongPlanet, TowardsPlanet, Space.Self);



			if (distToPlanet > maxDistanceFromPlanet) {
				transform.position -= (maxDistanceFromPlanet - distToPlanet) * directionOfPlanet.normalized;
			}
		} else {

			Vector3 ObjectToLookAtDirection = GameObject.FindGameObjectWithTag ("farObject").gameObject.transform.position-transform.position;
			float turnSpeed =  Time.deltaTime;
			Vector3 newDirection = Vector3.RotateTowards (transform.forward, ObjectToLookAtDirection, turnSpeed, 0.0f);
			transform.rotation = Quaternion.LookRotation (newDirection);
			transform.Translate (Vector3.forward);
			starMove.SetActive (true);

		
		}

	}
}
