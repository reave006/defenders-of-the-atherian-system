﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reasonsToDestroyTower : MonoBehaviour {

	public GameObject explosion;

	private BuildManager bm;

	void Start () {
		bm = GameObject.Find ("TowerManager").GetComponent<BuildManager>();
		
	}
	
	void OnTriggerEnter (Collider collided)
	{
		Debug.Log(gameObject.tag + " collided with " + collided.tag);
		if (collided.gameObject.tag == "Enemy") 
		{
			Vector3 explosionLocation = new Vector3(transform.position.x, transform.position.y, transform.position.z);
			Quaternion explosionRotation = transform.rotation;
			Instantiate(explosion,explosionLocation,explosionRotation);
			Destroy(gameObject);
		}

		if (collided.gameObject.tag == "base") 
		{
			bm.DeactivateBuildingmode();
			Destroy(gameObject);
		}

		// Don't allow towers to be placed on top of eachother.
		if (collided.gameObject.tag == "tower") 
		{
			bm.DeactivateBuildingmode();

			Vector3 explosionLocation1 = new Vector3(transform.position.x, transform.position.y, transform.position.z);
			Quaternion explosionRotation1 = transform.rotation;
			Instantiate(explosion, explosionLocation1, explosionRotation1);

			Vector3 explosionLocation2 = new Vector3(collided.transform.position.x, collided.transform.position.y, collided.transform.position.z);
			Quaternion explosionRotation2 = collided.transform.rotation;
			Instantiate(explosion, explosionLocation2, explosionRotation2);

			Destroy(collided);
			Destroy(gameObject);
		}
	}

	/// <summary>
	/// OnTriggerStay is called once per frame for every Collider other
	/// that is touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerStay(Collider other)
	{
		// Don't allow towers to be placed on top of eachother.
		if (other.gameObject.tag == "tower") 
		{
			bm.DeactivateBuildingmode();

			Vector3 explosionLocation1 = new Vector3(transform.position.x, transform.position.y, transform.position.z);
			Quaternion explosionRotation1 = transform.rotation;
			Instantiate(explosion, explosionLocation1, explosionRotation1);

			Vector3 explosionLocation2 = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
			Quaternion explosionRotation2 = other.transform.rotation;
			Instantiate(explosion, explosionLocation2, explosionRotation2);

			Destroy(other);
			Destroy(gameObject);
		}		
	}
}
