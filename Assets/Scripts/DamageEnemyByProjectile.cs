﻿/// <summary>
/// Handle when enemies are hit by projectiles.
/// @author Allan Haywood
/// </summary>
using UnityEngine;
using System.Collections;

public class DamageEnemyByProjectile : MonoBehaviour
{
    public int decreaseOnProjectile1Hit;

    public int decreaseOnProjectile2Hit;

    public int decreaseOnProjectile3Hit;

    public GameObject[] enemyExplosions;

    public GameObject healthBar;
    public int startingHealth;

    private int currentHealth;

    void Start()
    {
        currentHealth = startingHealth;
    }

	/// <summary>
	/// This function is called when the MonoBehaviour will be destroyed.
	/// </summary>
	void OnDestroy()
	{
		//When this enemy is destroyed, decrement remaining enemies. This call should only be done here.
		GameObject gameController = GameObject.FindGameObjectWithTag("GameController");
		if (gameController != null)
		{
            GameObject.Find("TowerManager").GetComponent<BuildManager>().enemyKilled();//tells the buildmanager than an enemy has been killed
			gameController.GetComponent<EndGameOnAllEnemysDead>().enemyHasDied("OnDestroy");
		}
	}

    /// When two objects collide, determine if if is a projectile and react accordingly.
    /// </summary>
    /// <param name="other">The other object that the enemy.</param>
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enemy hit by:" + other.tag);

        // If the contacted object is a projectile, destroy the other object, and subtract from enemy health.
        // If enemy health reaches zero, destroy the enemy.
        if (other.tag == "Projectile1")
        {
            //audioSource[1].Play();
            //Destroy(other.gameObject); 
            currentHealth -= decreaseOnProjectile1Hit;

            Debug.Log("Enemy Health:" + currentHealth);
            SetHealthBar(startingHealth, currentHealth);
        }

        // If the contacted object is a projectile, destroy the other object, and subtract from enemy health.
        // If enemy health reaches zero, destroy the enemy.
        if (other.tag == "Projectile2")
        {
            //audioSource[1].Play();
            //Destroy(other.gameObject);
            currentHealth -= decreaseOnProjectile2Hit;

            Debug.Log("Enemy Health:" + currentHealth);

            SetHealthBar(startingHealth, currentHealth);
        }

        // If the contacted object is a projectile, destroy the other object, and subtract from enemy health.
        // If enemy health reaches zero, destroy the enemy.
        if (other.tag == "Projectile3")
        {
            //audioSource[1].Play();
            //Destroy(other.gameObject);
            currentHealth -= decreaseOnProjectile3Hit;

            Debug.Log("Enemy Health:" + currentHealth);


            SetHealthBar(startingHealth, currentHealth);
        }

		// If enemy health is zero, destroy the enemy.
		if (currentHealth <= 0)
		{
			DestroyThisEnemy();
		}		
    }

    void SetHealthBar(int startingHealth, int currentHealth)
    {
        float barScale = (float)currentHealth / (float)startingHealth;
        Debug.Log("HealthBarScale:" + barScale);
        healthBar.transform.localScale = new Vector3(barScale, 1, 1);
    }

    void DestroyThisEnemy()
    {
        Vector3 explosionLocation = this.gameObject.transform.position;
        Quaternion explosionRotation = this.gameObject.transform.rotation;


        int index = 0;
        if (Random.value > .9)
        {
            index = 1;
        }
        Instantiate(enemyExplosions[index], explosionLocation, explosionRotation);

        Destroy(this.gameObject);
    }
}
