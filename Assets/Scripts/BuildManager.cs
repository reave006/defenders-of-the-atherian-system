 using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Build manager.
/// 
/// This Script is attached to a empty GameObject
/// </summary>

[System.Serializable]
public class Snapping
{
    public bool snappingEnabled = false;
    public float snapRadius = 2.0f;
    public float snapMagin = 0.0f;
    public SnapSides snapSides;


}

[System.Serializable]
public struct SnapSides
{
    // based on objects facing direction
    public bool left, right, top, bottom, front, back;
}

public class BuildManager: MonoBehaviour 
{
	public int SelectedBuilding;
	public GameObject ThingToLookAt;//added by JoshuaReaves CS485
	public GameObject[] Building;
	public int TowersRemaining;
	//public string BaseCollisionTag;
	public List<BuildingList> buildings = new List<BuildingList>();
	public string TerrainCollisionTag;
    //public float maxSlopeHigh = 5f;
    public bool isBuildingEnabled { get; private set; }

	public int howManyEnemiesToKillBeforeTower;
	
	private int LastSelectedBuilding;
    private GameObject ghost;
    private BuildingCollision ghostCollision;
	private bool isFlat;
	private Vector3 origin = new Vector3(0,0,0);
	private int enemiesKilled=0;
	private bool enemyJustKilled=true;
    private int maxTowerResources;

    // Unused until fully implemented
   // public Snapping snapping = new Snapping();

    void Start()
	{
		LastSelectedBuilding = SelectedBuilding;
        isBuildingEnabled = false;

        // Set starting towers remaining to max resources, so that when enemeis are killed,
        // There is a limit to how many resources can be stored.
        maxTowerResources = TowersRemaining;
    }

    public void ActivateBuildingmode()
    {
		Debug.Log ("Building Mode activated");
		isBuildingEnabled = true;
    }

    public void DeactivateBuildingmode()
    {
		Debug.Log ("Building Mode deactivated");
        isBuildingEnabled = false;
    }

    public void SelectBuilding(int id)
    {
		Debug.Log ("selecting a building");
		Debug.Log (id);
        if (id < Building.Length && id >= 0)
        {
            LastSelectedBuilding = SelectedBuilding;
            SelectedBuilding = id;
        }
    }

	public void enemyKilled()
	{
		enemiesKilled++;
		enemyJustKilled = true;
	}

    void Update()
	{
		if (enemiesKilled > 0) {
			if (enemiesKilled % howManyEnemiesToKillBeforeTower == 0 && enemyJustKilled == true) {
		
				enemyJustKilled = false;
                if (TowersRemaining < maxTowerResources)
                {
                    TowersRemaining++;
                }
				
			}
		}
		
        if (!isBuildingEnabled)
        {

            if (ghost != null)
            {
                Destroy(ghost);

            }
                
            return;
        }
            
		RaycastHit hoverHit;
        bool isHoverHit;

		Ray pointerRay = Camera.main.ScreenPointToRay(Input.mousePosition);


        // Get location of hit for hover point.
        isHoverHit = Physics.Raycast(pointerRay, out hoverHit, Mathf.Infinity, 1024, QueryTriggerInteraction.Collide); //1024 is the hover layer

        // Get anything that was hit.
        RaycastHit[] allHit = Physics.RaycastAll(pointerRay, Mathf.Infinity);

        // Temporarily assign hover hit.
        RaycastHit planetHit = hoverHit;
        bool isPlanetHit = false;

        // If the ghost is hovering, activate it and calculate the raycast from where it is hovering, to the planet surface.
        if(isHoverHit)
        {
            // If there is a ghost building, make it active when hovering.
            if (ghost != null)
            {
                ghost.SetActive(true);
            }

            // Get direction from hover hit to planet surface.
            Vector3 direction = (origin.normalized - hoverHit.point);
            Ray placementRay = new Ray(hoverHit.point, direction);

            // Calculate the position of the planet surface, from where the ghost is hovering.
            isPlanetHit = Physics.Raycast(placementRay, out planetHit, Mathf.Infinity, 512); //512 is the planet layer.

            // If the planet is hit, draw a line from hover position to planet surface where it will be placed.
            if (isPlanetHit)
            {
                //Debug.Log("Hover hit" + hoverHit.point);
                //Debug.Log("Planet hit" + planetHit.point);
                StartCoroutine(drawLine(hoverHit.point, planetHit.point, Color.green, .1f));
            }
        }
        else
        {
            // If there is a ghost, make it inactive when not hovering.
            if (ghost != null)
            {
                ghost.SetActive(false);
            }            
        }

        // Loop through all detected hits.
        foreach(RaycastHit hit in allHit)
        {
            // If the building has changed, destroy the ghost and return.
            if (SelectedBuilding != LastSelectedBuilding && ghost != null)
            {
                Destroy(ghost);
                LastSelectedBuilding = SelectedBuilding;

                break;
            }
            
            // If ghost is null, create a new one.
            if (ghost == null)
            {
                ghost = (GameObject)Instantiate(Building[SelectedBuilding], origin, Quaternion.identity);

                // Make ghost inactive for now, it will become active once the mouse is over the hover area.
                ghost.SetActive(false);

                ghost.name = ghost.name.Replace("(Clone)", "(Ghost)");

                //ghostCollision = ghost.AddComponent<BuildingCollision>();
            }


            // If the button was clicked when the ghost is active (hovering over the planet), destroy the ghost and create the permanent tower.
            if (Input.GetMouseButtonDown(0) && ghost.active)
            {
                BuildingList bl = new BuildingList();
                Quaternion ghostRotation = ghost.transform.rotation;//added by Josh to ensure towers are placed in the correct orientation
                Destroy(ghost);

                //Debug.Log("Hover hit" + hoverHit.point);
                //Debug.Log("Planet hit" + planetHit.point);

                bl.buildingGameObject = (GameObject)Instantiate(Building [SelectedBuilding],
                    new Vector3 (planetHit.point.x,
                        planetHit.point.y, //+ Building[SelectedBuilding].GetComponent<Collider>().transform.localScale.y / 2,
                        planetHit.point.z), ghostRotation);

                TowersRemaining--;

                string s = bl.buildingGameObject.name.Replace("(Clone)", "");
                bl.buildingGameObject.name = s;
                bl.buildingName = s;
                bl.buildingGameObject.AddComponent<BuildingCollision>();
				var script = bl.buildingGameObject.GetComponent<BulletTower>();
                if (script == null)
                {
                    // The machine gun turrent using a different script.
                    var machineGunTowerScript = bl.buildingGameObject.GetComponent<doubleTurretTower>();
                    machineGunTowerScript.enabled = true;
                }
                else
                {
                    script.enabled = true;
                }

                buildings.Add(bl);

                DeactivateBuildingmode();

                break;
            }

            // Set initial target position to origin, before it is calcualted.
            Vector3 ghostTargetPos = origin;

            // If it is hovering, set the position to the hover point.
            if(isHoverHit)
            {
                ghostTargetPos = new Vector3(
                    hoverHit.point.x,
                    hoverHit.point.y,// + Building[SelectedBuilding].GetComponent<Collider>().transform.localScale.y / 2,
                    hoverHit.point.z);
            }

            // If the ghost is not null, change its position to the target position.
            if (ghost != null)
            {
                ghost.transform.position = ghostTargetPos;

                // Rotate ghost to be uprgith on surface.
                ghost.transform.LookAt(ThingToLookAt.transform.position);

                ghost.GetComponent<Renderer>().material.CopyPropertiesFromMaterial(Building[SelectedBuilding].GetComponent<Renderer>().sharedMaterial);
                ghost.GetComponent<Renderer>().material.color = new Color(
                    0f,
                    1f, 
                    0f, 
                    0.6f);
            }
		}
    }

    IEnumerator drawLine(Vector3 start , Vector3 end, Color color,float duration = 0.2f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material (Shader.Find("Particles/Additive"));
        lr.SetColors(color,color);
        lr.SetWidth(.5f,.5f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        yield return new WaitForSeconds(duration);
        GameObject.Destroy(myLine);
    }    
}
	
