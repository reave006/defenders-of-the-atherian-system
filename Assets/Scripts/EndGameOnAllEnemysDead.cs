﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameOnAllEnemysDead : MonoBehaviour {

	private int enemiesRemaining;
	public GameObject WinItems;


	// Use this for initialization

	void Start () {
		enemiesRemaining = gameObject.GetComponent<GameController>().enemyCount;
		Debug.Log ("There are " + enemiesRemaining + " enemies at the start of the level");

	}

	// Update is called once per frame
	void Update () {


		if (((enemiesRemaining <= 0) && (gameObject.GetComponent<GameController>().baseHealth >1)) || Input.GetKey(KeyCode.C)) 
		{
				
			GameObject[] ships = GameObject.FindGameObjectsWithTag("EnemyShip");

			foreach (GameObject ship in ships) 
			{
				ship.GetComponent<BlowUpShip>().destroyTheShipWithFireball();
			}
				
			GameObject.Find("PlayerAdvanced").GetComponent<ObjectMover>().hasWon = true;
			WinItems.SetActive(true);
		}

	}

	public void enemyHasDied(string whoToldMe)
	{
		enemiesRemaining--;
		Debug.Log (whoToldMe + " said an enemy has died"+" there are " + enemiesRemaining + " enemies remaining");
	}


	public void advanceToNextLevel()
	{
		Scene theCurrentScene = SceneManager.GetActiveScene ();
		string currentScene = theCurrentScene.name;

		if (currentScene == "firstPlanet") {
			
			GameObject.Find ("LevelRestriction").GetComponent<AccessToLevels> ().accessToPlanet2 = true;
			SceneManager.LoadScene ("SecondPlanet");

		} else if (currentScene == "SecondPlanet") {

			GameObject.Find ("LevelRestriction").GetComponent<AccessToLevels> ().accessToPlanet3 = true;
			SceneManager.LoadScene ("thirdPlanet");

		} else if (currentScene == "thirdPlanet") {

			SceneManager.LoadScene ("_Menu");

		} else {
			Debug.Log ("Something went wrong while going to the next scene");
		}
	}

	public void goBackToMenu()
	{
		SceneManager.LoadScene ("_Menu");
	}

	public void goBackToMenuAfterWin()
	{
		Scene theCurrentScene = SceneManager.GetActiveScene ();
		string currentScene = theCurrentScene.name;

		if (currentScene == "firstPlanet") {
			GameObject.Find("LevelRestriction").GetComponent<AccessToLevels>().accessToPlanet2 = true;
		} else if (currentScene == "SecondPlanet") {
			GameObject.Find("LevelRestriction").GetComponent<AccessToLevels>().accessToPlanet3 = true;
		}

		goBackToMenu ();
	}


}
