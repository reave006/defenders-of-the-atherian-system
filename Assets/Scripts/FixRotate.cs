﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FixRotate : MonoBehaviour {

	Renderer[] renderers;
	CanvasRenderer[] canvases;

	// Update is called once per frame
	void Start() 
	{
		// Disable all renderers until rotation is fixed.
		renderers = gameObject.GetComponentsInChildren<Renderer>();
		foreach(Renderer renderer in renderers)
		{
			Debug.Log("Disabling:" + renderer.name);
			renderer.enabled = false;
		}

		canvases = gameObject.GetComponentsInChildren<CanvasRenderer>();
		foreach(CanvasRenderer renderer in canvases)
		{
			Debug.Log("Disabling:" + renderer.name);
			renderer.SetAlpha(0f);
		}
		

		StartCoroutine(DoRotation());
	}

	IEnumerator DoRotation()
	{
		if (gameObject.GetComponent<SWS.splineMove>().pathContainer != null)
		{
			float xEnemyRotation = gameObject.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().xEnemyRotation;
			float yEnemyRotation = gameObject.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().yEnemyRotation;
			float zEnemyRotation = gameObject.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().zEnemyRotation;

			// May not be needed.
			//float xHealthBarPosition = gameObject.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().xHealthBarPosition;
			//float yHealthBarPosition = gameObject.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().yHealthBarPosition;
			//float zHealthBarPosition = gameObject.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().zHealthBarPosition;

			yield return new WaitForSeconds(.1f);
			//Debug.Log("Updating rotation:" + xEnemyRotation + "," + yEnemyRotation + "," + zEnemyRotation);

			Vector3 enemyRotation = new Vector3(xEnemyRotation, yEnemyRotation, zEnemyRotation);
			this.gameObject.transform.Rotate(enemyRotation);

			// Update effect rotation to be local to enemy.
			Transform transportEffect = FindFirstChildByTag("TransportEffect");
			transportEffect.localRotation = Quaternion.identity;

			// Also rotate health bar.
			RectTransform HealthBar = this.gameObject.GetComponentInChildren<RectTransform>();
			HealthBar.transform.localRotation = Quaternion.identity;

			//Vector3 healthBarPosition = new Vector3(xHealthBarPosition, yHealthBarPosition, zHealthBarPosition);
			Vector3 healthBarPosition = new Vector3(0f, 8.5f, 0f);
			HealthBar.transform.localPosition = healthBarPosition;

			CapsuleCollider enemyCollider = this.gameObject.GetComponentInChildren<CapsuleCollider>();
			enemyCollider.center = new Vector3(0f, 3.63f, 0f);
			enemyCollider.direction = 1;
		}


		// Enable all renderers.
		foreach(Renderer renderer in renderers)
		{
			Debug.Log("Enabling:" + renderer.name);
			renderer.enabled = true;
		}

		foreach(CanvasRenderer renderer in canvases)
		{
			Debug.Log("Enabling:" + renderer.name);
			renderer.SetAlpha(1f);
		}		
	}

     /// <summary>
     /// Find all children of the Transform by tag (includes self)
     /// </summary>
     /// <param name="tags"></param>
     /// <returns></returns>
     Transform FindFirstChildByTag(string tag)
     {
		 
		 Transform[] tempList = gameObject.GetComponentsInChildren<Transform>();

         foreach (Transform transform in tempList)
		 {
			 if (transform.tag == tag)
			 {
				 return transform;
			 }
		 }

		 return null;
     }
}
