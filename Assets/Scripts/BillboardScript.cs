﻿using UnityEngine;
using System.Collections;

public class BillboardScript : MonoBehaviour 
{
	GameObject mainCamera;
	void Start()
	{
		mainCamera = GameObject.FindWithTag("MainCamera");
	}


	void Update () 
	{
		transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.back, mainCamera.transform.rotation * Vector3.up);
	}
}
