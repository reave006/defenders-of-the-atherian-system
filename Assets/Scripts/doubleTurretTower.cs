﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class doubleTurretTower : MonoBehaviour {

	public float range = 15f;
	public float fireRate = 1f;
	public float turnSpeed = 50f;
	public GameObject bulletPrefab1;
	public Transform bulletSpawnPoint1;
	public GameObject bulletPrefab2;
	public Transform bulletSpawnPoint2;


	private const int SLOW_SLEW_SPEED = 1;
	private const int FAST_SLEW_SPEED = 4;
	private float fireCountdown = 0f;
	private Transform target;
	private Transform turretTop;



	void Start()
	{
		Transform[] transforms = GetComponentsInChildren<Transform>();
		foreach (Transform t in transforms)
		{
			if (t.tag == "TurretTop")
			{
				turretTop = t;
				break;
			}
		}

		InvokeRepeating ("UpdateTarget", 0f, 0.5f);
	}

	void UpdateTarget()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		foreach (GameObject enemy in enemies) 
		{
			float distance = Vector3.Distance (transform.position, enemy.transform.position);
			if (distance < shortestDistance) 
			{
				shortestDistance = distance;
				nearestEnemy = enemy;
			}
		}

		if (nearestEnemy != null && shortestDistance <= range) {
			target = nearestEnemy.transform;
		} else {
			target = null;
		}
	}
	void FixedUpdate()
	{
		if (target == null)
			return;

		//Get vector that points to location of closest enemy.
		var heading = (target.position - turretTop.position).normalized;
		//Debug.Log("Heading Z:" + heading.z);

		// Get angle from turret for the above heading.
		// Pointing at the enemy turns out to be 90degrees for some reason.
		// This may be due to the program the model was built in.
		float angle = Vector3.Angle( heading, turretTop.up * -1);
		
		// Centering value on 0 by subrtracting 90, not sure why this works out to 90, couldn't find a setting that made it 0.
		angle = angle - 90;
		//Debug.Log("Angle:" + angle);

		if (angle > 0)
		{
			Debug.Log("Rotate Right");
			if (angle > 10)
			{
				Debug.Log("Tower slewing fast");
				turretTop.Rotate(0, 0, FAST_SLEW_SPEED); //Fast slew
			}
			else
			{
				Debug.Log("Tower slewing slow");
				turretTop.Rotate(0, 0, SLOW_SLEW_SPEED); //slow slew
				Shoot();
			}
			
		}
		else if (angle < 0)
		{
			Debug.Log("Rotate Left");
			if (angle < -10)
			{
				Debug.Log("Tower slewing fast");
				turretTop.Rotate(0, 0, FAST_SLEW_SPEED * -1);	//Fast slew
			}
			else
			{
				Debug.Log("Tower slewing slow");
				turretTop.Rotate(0, 0, SLOW_SLEW_SPEED * -1); //Slow slew
				Shoot();
			}
		}
	}

	void Shoot()
	{
		if (fireCountdown <= 0f) 
		{
			GameObject bulletGO1 = (GameObject)Instantiate (bulletPrefab1, bulletSpawnPoint1.position, bulletSpawnPoint1.rotation);
			GameObject bulletGO2 = (GameObject)Instantiate (bulletPrefab2, bulletSpawnPoint2.position, bulletSpawnPoint2.rotation);

			BulletBehavior bullet1 = bulletGO1.GetComponent<BulletBehavior> ();
			BulletBehavior bullet2 = bulletGO2.GetComponent<BulletBehavior> ();


			if (bullet1 != null)
				bullet1.SeekEnemy (target);
			if (bullet2 != null)
				bullet2.SeekEnemy (target);

			fireCountdown = 1f / fireRate;
		}

		fireCountdown -= Time.deltaTime;		
		
	}
}