﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	public float speed;
	private Renderer rend;
	// Use this for initialization

	void Start () {
		rend = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		float offset = Time.time * speed;
		rend.material.SetTextureOffset("_MainTex",new Vector2(offset,0));
	
	}
}


