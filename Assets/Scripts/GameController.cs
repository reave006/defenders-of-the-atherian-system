﻿/// <summary>
/// Handles coordinating events accross the scnene.
/// @author Allan Haywood
/// </summary>
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using DG.Tweening;
using SWS;

/// <summary>
/// The game controller that coordinates most aspects the game.
/// </summary>
public class GameController : MonoBehaviour 
{
	public SWS.PathManager[] paths;
	public SWS.PathManager[] shipPaths;
	public GameObject enemy;
	public GameObject enemyShip;
	public float startWait;
	public float spawnWait;
    //public Text scoreText;
    //private int score;
    //private bool gameOver;
	public int baseHealth;
	public int enemyCount;

	private double timeLeft; 

	private Text baseHealthText;
	private Text timeCounterText;

	private GameObject[] spawnPoints;

	/// <summary>
    /// At the start of the scene, initialize the values, and start the games coroutine. 
    /// </summary>
	void Start()
	{
		spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
		foreach(GameObject spawnPoint in spawnPoints)
		{
			spawnPoint.active = false;
		}

        //gameOver = false;
		//score = 0;
		//UpdateScore(score);
		baseHealthText = GameObject.FindGameObjectWithTag("HealthCounter").GetComponent<Text>();
		baseHealthText.text = baseHealth.ToString();
		StartCoroutine(SpawnWaves());

		timeLeft = startWait;
		timeCounterText = GameObject.FindGameObjectWithTag("TimeCounter").GetComponent<Text>();
		timeCounterText.text = timeLeft.ToString();


	}

	void Update()
	{
		// Update the countdown timer for when the enemies arrive.
		timeLeft -= Time.deltaTime;
		if (timeLeft < 0)
		{
			timeLeft = 0.0;
		}
		
		timeCounterText.text = timeLeft.ToString();		
	}


	/// <summary>
    /// Spawns enemies. 
    /// </summary>
    /// <returns></returns>
	IEnumerator SpawnWaves()
	{
		// Start ship approach.
		// They will always arrive when the startWait timer runs out.
		foreach (PathManager shipPath in shipPaths)
		{
			GameObject newShip = Instantiate(enemyShip, new Vector3(1000000,0,0), Quaternion.identity) as GameObject;

			newShip.GetComponent<splineMove>().SetPath(shipPath);
			newShip.GetComponent<splineMove>().speed = startWait;
		}

		yield return new WaitForSeconds(startWait);

		foreach(GameObject spawnPoint in spawnPoints)
		{
			spawnPoint.active = true;
		}

		yield return new WaitForSeconds(2);

		// Enemy is first instantiated in the middle of the planet, but then moved to one of the paths.
		Vector3 pos = new Vector3(0,0,0);

		// Loop for the number of projectiles in this wave. 
		for (int i = 0; i < enemyCount; i++)
		{
			
			// Spawn a new enemy.
			GameObject newEnemy = Instantiate(enemy, pos, Quaternion.identity) as GameObject;

			// Randomly assign the enemy to one of the available enemy paths.
			newEnemy.GetComponent<splineMove>().SetPath(paths[Random.Range(0, paths.Length)]);
			Debug.Log ("An enemy has spawned, enemy count at " + enemyCount);

			/*
			float x = newEnemy.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().x;
			float y = newEnemy.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().y;
			float z = newEnemy.GetComponent<SWS.splineMove>().pathContainer.GetComponent<PathCorrection>().z;

			Debug.Log("Updating rotation:" + gameObject.name + "x:" + x + "y:" + y + "z:" + z);

			Vector3 vector3 = new Vector3(x, y, z);
			newEnemy.transform.Rotate(vector3);			
*/			
			// Wait for the specified amount of time before spawying the next enemy.
			yield return new WaitForSeconds(spawnWait);
		}
	}

	/// <summary>
    /// This is called if an enemy reaches the base, to decrease the base health.
    /// </summary>
    /// <param name="health">The amount of health to decrease by.</param>
	public void DecreaseHealth(int health)
	{
		baseHealth -= health;
		baseHealthText.text = baseHealth.ToString();
	}

	public void goBackToMainMenu()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("_Menu");
	}
		
}
