﻿using UnityEngine;
using System.Collections;

public class SizeChange : MonoBehaviour {
	
	private int x;
	private float slope;

	// Use this for initialization
	void Start () {
		x = GameObject.Find ("TowerManager").GetComponent<BuildManager> ().TowersRemaining;
		slope = 100.0f/x;
	}

	// Update is called once per frame
	void Update () {
		x = GameObject.Find ("TowerManager").GetComponent<BuildManager> ().TowersRemaining;

		var local = (slope * x)/100.0f;
		transform.localScale = new Vector3 ((0.23f*local), (0.23f*local), 0.05f);

	}
}

