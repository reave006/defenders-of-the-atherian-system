﻿using UnityEngine;
using System.Collections;

public class CameraControl: MonoBehaviour {
	public GameObject HomePlanet;
	//public float maxDistanceFromPlanet;
	public float strength= 0.5F;

	GameObject character;

	// Use this for initialization
	void Start () {
		character = this.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Quaternion targetRotation = Quaternion.LookRotation (HomePlanet.transform.position - transform.position);
		var str = Mathf.Min (strength * Time.deltaTime, 1);
		transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, str);

		character.transform.localRotation = Quaternion.AngleAxis(targetRotation.x, character.transform.up);
	}
}
