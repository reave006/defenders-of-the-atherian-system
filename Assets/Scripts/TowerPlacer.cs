using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TowerPlacer : MonoBehaviour {

	BuildManager bm;
	//Button towerButton;

	// Use this for initialization
	void Start () {
		bm = GameObject.Find ("TowerManager").GetComponent<BuildManager> ();
	}


	public void ActiveteBuilding(Button pressedBtn)
	{
		if (GameObject.Find ("TowerManager").GetComponent<BuildManager>().TowersRemaining > 0) {
			Debug.Log ("here");
			if (pressedBtn.name == "MachineGunButton") {
				Debug.Log ("machine gun button was pressed");
				bm.SelectBuilding (0);
				bm.ActivateBuildingmode ();
			}
			if (pressedBtn.name == "PlaceTowerButton") {
				Debug.Log ("tower button was pressed");
				bm.SelectBuilding (1);
				bm.ActivateBuildingmode ();
			}
			if (pressedBtn.name == "PlacePillarButton") {
				bm.SelectBuilding (2);
				bm.ActivateBuildingmode ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}